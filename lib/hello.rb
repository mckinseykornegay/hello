# Default is "World"
# Author: McKinsey Kornegay (jkorneg2@uncc.edu)
name = ARGV.first || "World"
puts “Hello, #{name}!”
